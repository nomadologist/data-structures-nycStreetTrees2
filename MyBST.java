import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
/**
 * Generic implementation of a binary search tree
 * 
 * @author Jack Booth
 * @version 4/23/2017
 */
public class MyBST<E extends Comparable <E>>
{
	protected BSTNode<E> root;
	
	/**
	 * Adds the specified element if it isn't already in the BST
	 * @param e element to add
	 * @return true if the BST didn't already contain the element
	 * @throws ClassCastException if specified element can't be compared with elements in the BST
	 * @throws NullPointerException if specified element is null
	 */
	public boolean add(E e) throws ClassCastException, NullPointerException
	{
		if(e == null)
			throw new NullPointerException();
		try{
		root = add(root, e);
		}
		catch(IllegalArgumentException f){
			return false;
		}
		return true;
	}
	
	/**
	 * Recursive wrapper used to add data to the BST
	 * @param node current node being explored
	 * @param data data to add
	 * @return the node being added in the correct place in the BST
	 * @throws IllegalArgumentException if the element is already in the BST
	 */
	protected BSTNode<E> add(BSTNode<E> node, E data) throws IllegalArgumentException
	{
		if(node!=null && data.compareTo(node.getData())==0)
			throw new IllegalArgumentException();
		
		if(node == null)
			node = new BSTNode<E>(data);
			
		else if(data.compareTo(node.getData()) < 0)
			node.setLeft(add(node.getLeft(), data));
		
		else if((data.compareTo(node.getData()) > 0))
			node.setRight(add(node.getRight(), data));
		
		return node;
	}
	
	/**
	 * Removes an element from the BST
	 * @param o object to remove
	 * @return true if element was found and removed
	 * @throws ClassCastException if the element specified isn't of the type of the BST
	 * @throws NullPointerException if object specified is null
	 */
	@SuppressWarnings("unchecked")
	public boolean remove(Object o) throws ClassCastException, NullPointerException
	{	
		if(o == null)
			throw new NullPointerException();
		
		BSTNode<E> tmp = remove(root, (E)o);
		if(tmp == null)
		{
			return false;
		}
		
		root = tmp;
		return true;
	}
	
	/**
	 * Wrapper to recursively remove nodes
	 * @param node current node
	 * @param data data to remove
	 * @return the next node to be explored
	 */
	protected BSTNode<E> remove(BSTNode<E> node, E data)
	{
//		returns null if item is not in the tree.
		if(node == null)
		{
			return null;
		}
		
		else if(data.compareTo(node.getData()) < 0)
			node.setLeft(remove(node.getLeft(), data));
		
		else if(data.compareTo(node.getData()) > 0)
			node.setRight(remove(node.getRight(), data));
		
		else
			node = remove(node);
		
		return node;
	}
	
	/**
	 * Second wrapper class, which actually removes the node based on its successor
	 * @param node current node
	 * @return the next node to explore
	 */
	private BSTNode<E> remove(BSTNode<E> node)
	{
		if(node.getLeft() == null)
			return node.getRight();
		
		if(node.getRight() == null)
			return node.getLeft();
		
		E data = getSuccessor(node);
		node.setData(data);
		node.setLeft(remove(node.getLeft(), data));
		return node;
	}
	
	/**
	 * finds the successor of the specified node
	 * @param node the node to find the successor of 
	 * @return the succeeding node that will replace the specified node
	 */
	private E getSuccessor(BSTNode<E> node)
	{
		BSTNode<E> successor = null;
		BSTNode<E> current = node.getRight();
		
//		Traverses as far left down the node as possible
		while(current!=null)
		{
			successor = current;
			current = current.getLeft();
		}
		
		return successor.getData();
	}
	
	/**
	 * Sees if the BST contains the specified object.
	 * @param o object to search for
	 * @return true if the BST contains o
	 * @throws ClassCastException if o is not of the type of the BST
	 * @throws NullPointerException if o is null
	 */
	@SuppressWarnings("unchecked")
	public boolean contains(Object o) throws ClassCastException, NullPointerException
	{
		if(o == null)
			throw new NullPointerException();
		
		return contains(root, (E)o);
	}
	
	/**
	 * Recursive wrapper method to search through the tree for an object
	 * @param currentNode the current node that is being explored
	 * @param data the data to search for
	 * @return true if data is found
	 */
	private boolean contains(BSTNode<E> currentNode, E data)
	{
		if(currentNode == null)
			return false;
		
		else if(data.compareTo(currentNode.getData()) < 0)
			return(contains(currentNode.getLeft(), data));
		
		else if(data.compareTo(currentNode.getData()) > 0)
			return(contains(currentNode.getRight(), data));
		
		else
			return true;
	}
	
	/**
	 * Finds the first(smallest) element in the BST
	 * @return the first node's data
	 * @throws NoSuchElementException if the root is null
	 */
	public E first() throws NoSuchElementException
	{
		if(root == null)
			throw new NoSuchElementException();
		
		BSTNode<E> current = root;
		while(current.getLeft()!=null)
			current = current.getLeft();
		return current.getData();
	}
	
	/**
	 * finds the last(largest) element in the BST
	 * @return the last node's data
	 * @throws NoSuchElementException if the root is null
	 */
	public E last() throws NoSuchElementException
	{
		if(root == null)
			throw new NoSuchElementException();
		
		BSTNode<E> current = root;
		while(current.getRight()!=null)
			current = current.getRight();
		return current.getData();
	}
	
	@Override
	public String toString()
	{
		ArrayList<E> list = inOrderTraversal(root);
		Collections.sort(list);
		return list.toString();
	}
	
	/**
	 * Wrapper method that traverses the BST in inorder order and creates a list of every node
	 * @param node the current node being explored
	 * @return an ArrayList of every node in the BST
	 */
	protected ArrayList<E> inOrderTraversal(BSTNode<E> node)
	{
		ArrayList<E> list = new ArrayList<E>();
		if(node!=null)
		{
			list.addAll(inOrderTraversal(node.getLeft()));
			list.add(node.getData());
			list.addAll(inOrderTraversal(node.getRight()));
		}
		return list;
	}
}
