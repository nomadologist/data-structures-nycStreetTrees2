/**
 * A Node class meant to be used with a binary search tree
 * 
 * @author Jack Booth
 * @version 4/23/2017
 */
public class BSTNode<E extends Comparable<E>> implements Comparable<BSTNode<E>>
{
	private BSTNode<E> right;
	private BSTNode<E> left;
	
	private E data;
	
	/**
	 * Constructor that sets generic data field
	 * @param data to set
	 */
	public BSTNode(E data)
	{
		this.data = data;
	}
	
	/**
	 * Points the right node
	 * @param j the node to set
	 */
	public void setRight(BSTNode<E> j)
	{
		right = j;
	}
	
	/**
	 * Points the left node
	 * @param j the node to set
	 */
	public void setLeft(BSTNode<E> j)
	{
		left = j;
	}
	
	/**
	 * Gets the right node
	 * @return what the node is pointing to on the right
	 */
	public BSTNode<E> getRight()
	{
		return right;
	}
	
	/**
	 * Gets the left node
	 * @return what the node is pointing to on the left
	 */
	public BSTNode<E> getLeft()
	{
		return left;
	}
	
	/**
	 * Gets the data contained in the node
	 * @return the data in the current node
	 */
	public E getData()
	{
		return data;
	}
	
	/**
	 * Changes the current node's data
	 * @param d the data to change
	 */
	public void setData(E d)
	{
		data = d;
	}

	@Override
	public int compareTo(BSTNode<E> arg0)
	{
		return this.data.compareTo(arg0.data);
	}

}
