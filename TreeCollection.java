import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
/**
 * A binary search tree specialized to the Tree class
 * 
 * @author Jack Booth
 * @version 4/23/2017
 */
public class TreeCollection extends MyBST<Tree>
{
	private int size = 0;
	private int brooklynCount = 0;
	private int bronxCount = 0;
	private int manhattanCount = 0;
	private int queensCount = 0;
	private int statenCount = 0;
	private ArrayList<String> uniqueSpecies = new ArrayList<String>();
	
	/**
	 * 	default constructor, creates an empty list
	 */
	public TreeCollection()
	{
		super();
	}
	
	/**
	 * Gets the total number of tree objects stored in this list
	 * @return the total number of tree objects
	 */
	public int getTotalNumberOfTrees()
	{
		return size;
	}
	
	/**
	 * Gets the total number of trees that match a certain species.  
	 * @param speciesName the name of the species that is being searched for.
	 * @return the number of trees that match a species, or 0 if called with a non-existent species
	 */
	public int getCountByTreeSpecies(String speciesName)
	{
		int count = 0;
//		Gets matching species and creates an iterator from that collection
		Collection<String> speciesCollection = getMatchingSpecies(speciesName);
		Iterator<String> iterator = speciesCollection.iterator();
		
//		Iterates through all matching species, and calls a function to search the tree and count the number of times that a node has the species name
		while(iterator.hasNext())
		{
			count += recTreeCount(root, iterator.next(), 0);
		}
		return count;
	}
	
	/**
	 * Wrapper method to recursively count the nodes which contain a specified species name
	 * @param node node being searched
	 * @param speciesName name being searched for
	 * @param count variable to keep track of count
	 * @return the count of nodes that contain speciesName
	 */
	private int recTreeCount(BSTNode<Tree> node, String speciesName, int count)
	{
//		if the current node has the species name, increments count then recursively checks left and right trees for the same species name
		if(node.getData().getSpecies().equalsIgnoreCase(speciesName))
		{
			count ++;
			if(node.getLeft()!=null)
				count += recTreeCount(node.getLeft(), speciesName, 0);
			
			if(node.getRight()!=null)
				count += recTreeCount(node.getRight(), speciesName, 0);
		}
		
//		if name of species comes before the current node's name, explores the left tree
		else if(speciesName.toLowerCase().compareTo(node.getData().getSpecies().toLowerCase())<0)
		{
			if(node.getLeft()!=null)
				count += recTreeCount(node.getLeft(), speciesName, 0);
		}
		
//		if name of species comes after the current node's name, explores the right tree
		else if(speciesName.toLowerCase().compareTo(node.getData().getSpecies().toLowerCase())>0)
		{
			if(node.getRight()!=null)
				count += recTreeCount(node.getRight(), speciesName, 0);
		}
		
		return count;
	}
	
	
	@Override
	public boolean add(Tree tree) throws ClassCastException, NullPointerException
	{
//		keeps track of size, how many trees boroughs have, and the number of unique species of trees
		size++;
		if(tree.getBoroName().equalsIgnoreCase("manhattan"))
			manhattanCount+=1;
		else if(tree.getBoroName().equalsIgnoreCase("brooklyn"))
			brooklynCount+=1;
		else if(tree.getBoroName().equalsIgnoreCase("bronx"))
			bronxCount+=1;
		else if(tree.getBoroName().equalsIgnoreCase("queens"))
			queensCount+=1;
		else if(tree.getBoroName().equalsIgnoreCase("staten island"))
			statenCount+=1;
		try{
//			Uses wrapper method to add tree to the BST
			root = add(root, tree);
			
			if(!uniqueSpecies.contains(tree.getSpecies().toLowerCase()))
			{
				uniqueSpecies.add(tree.getSpecies().toLowerCase());
			}
			}
//		if the tree already exists in the borough
		catch(IllegalArgumentException f){
			return false;
		}
			return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override 
	public boolean remove(Object o) throws ClassCastException, NullPointerException
	{
		if(o == null)
			throw new NullPointerException();
//		decrements size and counts for trees in each borough.
		if(((BSTNode<Tree>) o).getData().getBoroName().equalsIgnoreCase("manhattan"))
			manhattanCount--;
		else if(((BSTNode<Tree>) o).getData().getBoroName().equalsIgnoreCase("brooklyn"))
			brooklynCount--;
		else if(((BSTNode<Tree>) o).getData().getBoroName().equalsIgnoreCase("bronx"))
			bronxCount--;
		else if(((BSTNode<Tree>) o).getData().getBoroName().equalsIgnoreCase("queens"))
			queensCount--;
		else if(((BSTNode<Tree>) o).getData().getBoroName().equalsIgnoreCase("staten island"))
			statenCount--;
		
		size--;
		BSTNode<Tree> tmp = remove(root, (Tree) o);
		if(tmp == null)
		{
			return false;
		}
		
		root = tmp;
		return true;
	}
	
	/**
	 * Gets the total number of trees within a certain borough.
	 * @param boroName the borough to search for
	 * @return the number of trees that are in the specified borough, or 0 if called with a non-existent borough
	 */
	public int getCountByBorough(String boroName)
	{
		if(boroName.equalsIgnoreCase("Manhattan"))
			return manhattanCount;
		else if(boroName.equalsIgnoreCase("Bronx"))
			return bronxCount;
		else if(boroName.equalsIgnoreCase("Brooklyn"))
			return brooklynCount;
		else if(boroName.equalsIgnoreCase("Queens"))
			return queensCount;
		else if(boroName.equalsIgnoreCase("Staten Island"))
			return statenCount;
		else
			return 0;
	}
	
	/**
	 * Finds all nodes in the BST that match a species name
	 * @param speciesName the species being searched for
	 * @param list the list of nodes
	 * @param node the node that is being explored
	 * @return a list of BSTNodes, each of with have a tree that matches the specified species name
	 */
	private ArrayList<BSTNode<Tree>> findAllNodesSpecies(String speciesName, ArrayList<BSTNode<Tree>> list, BSTNode<Tree> node)
	{
		if(node!=null)
		{
//			If found node with matching species, adds it to the list and explores the left and right nodes
			if(node.getData().getSpecies().equalsIgnoreCase(speciesName))
			{
				list.add(node);
				list.addAll(findAllNodesSpecies(speciesName, new ArrayList<BSTNode<Tree>>(), node.getLeft()));
				list.addAll(findAllNodesSpecies(speciesName, new ArrayList<BSTNode<Tree>>(), node.getRight()));
			}
//			If node's species is less than the species being searched, explores right tree
			else if(node.getData().getSpecies().toLowerCase().compareTo(speciesName.toLowerCase())<0)
			{
				list.addAll(findAllNodesSpecies(speciesName, new ArrayList<BSTNode<Tree>>(), node.getRight()));
			}
//			If node's species is greater than the species being searches, explores left tree
			else if(node.getData().getSpecies().toLowerCase().compareTo(speciesName.toLowerCase())>0)
			{
				list.addAll(findAllNodesSpecies(speciesName, new ArrayList<BSTNode<Tree>>(), node.getLeft()));
			}
		}
		return list;
			
	}
	
	/**
	 * Method that finds the number of Tree objects in the list whose species matches the speciesName specified by the first 
	 * parameter and which are located in the borough specified by the second parameter
	 * @param speciesName the name of the species to search for
	 * @param boroName the name of the borough to search in
	 * @return returns number of tree found
	 */
	public int getCountByTreeSpeciesBorough ( String speciesName, String boroName )
	{
		int count = 0;
//		Generates a collection of all matching species
		Collection<String> speciesCollection = getMatchingSpecies(speciesName);
		Iterator<String> iterator = speciesCollection.iterator();
		ArrayList<BSTNode<Tree>> list = new ArrayList<BSTNode<Tree>>();
		
//		Iterates through that collection and adds all nodes that contain each unique species name to a list
		while(iterator.hasNext())
		{
			String name = iterator.next();
			list.addAll(findAllNodesSpecies(name, new ArrayList<BSTNode<Tree>>(), root));
		}
		
//		Iterates through that list and increments count when the node has a Tree with the specified borough name
		Iterator<BSTNode<Tree>> treeIterator = list.iterator();
		while(treeIterator.hasNext())
		{
			BSTNode<Tree> node = treeIterator.next();
			if(node.getData().getBoroName().equalsIgnoreCase(boroName))
			{
				count++;
			}
		}
		return count;
	}

	/**
	 * Finds all the tree species that match the specified speciesName (are a substring of speciesName).
	 * @param speciesName the species of the tree to search for
	 * @return A Collection<String> object containing a list of all matching tree species.
	 */
	public Collection<String> getMatchingSpecies(String speciesName)
	{
		Collection<String> matchingSpecies = new ArrayList<String>();
		for(int i = 0; i<uniqueSpecies.size(); i++)
		{
			if(uniqueSpecies.get(i).toLowerCase().contains(speciesName.toLowerCase()))
			{
				matchingSpecies.add(uniqueSpecies.get(i));
			}
		}
		return matchingSpecies;
	}
	
	@Override
	public String toString()
	{
		ArrayList<Tree> list = inOrderTraversal(root);
		Collections.sort(list);
		return "Tree: " + list.toString();
	}
}
